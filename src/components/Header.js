import React from 'react';

const Header = props => {
  return (
    <nav className="navbar navbar-dark bg-info">
      <a className="navbar-brand" href="/">Generic Survey</a>
      <small>{props.title}</small>
    </nav>
  );
}

export default Header;