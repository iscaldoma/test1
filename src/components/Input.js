import React from 'react';

const Input = props => {
  const handleChange = (event) => { 
    const name = event.target.name;    
    const value = event.target.value;    
    props.onChange(name, value);
  }

  return (
    <div className="form-group">
      <label>{props.label}</label>
      <input type={props.type} name={props.name} className="form-control" onChange={handleChange} />
    </div>
  );
}

export default Input;