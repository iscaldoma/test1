import React from 'react';
import styles from '../App.module.css';

const Questions = props => {
  const dataSurvey = [
    {question: "What software tools are used in the company?", answer:"", id: 1},
    {question: "Do you work with some kind of standard / code style manual?", answer:"", id: 2},
    {question: "What database technologies does the company work with?", answer:"", id: 3},
    {question: "What operating systems are used in the company?", answer:"", id: 4},
    {question: "Is the programming team AGILE or do they follow a cascading methodology?", answer:"", id: 5}    
  ];

  const handleInput = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    const id = event.target.id;

    props.handleInputValue(name, value, id);
  }

  const items = dataSurvey.map((item, i) => 
    <div className={`col-md-12 ${styles.surveyItem}`} key={i}>
      <div>{item.question}</div>
      <input className={styles.surveyInput} name={"question"+item.id} id={item.id} type="text" onChange={handleInput}/>
    </div>    
  );

  return (
    <div className="row">
      {items}
    </div>    
  );
}

export default Questions;