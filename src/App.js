import React, { useState } from 'react';
import Header from './components/Header';
import Questions from './components/Questions';

function App() {    
  const [question1, setQuestion1] = useState();
  const [question2, setQuestion2] = useState();
  const [question3, setQuestion3] = useState();
  const [question4, setQuestion4] = useState();
  const [question5, setQuestion5] = useState();
  const handleInputValue = (name, value, id) => {      
    if (id === "1") {    
      setQuestion1(value);
    }
    if (id === "2") 
      setQuestion2(value);
    if (id === "3") 
      setQuestion3(value);
    if (id === "4") 
      setQuestion4(value);
    if (id === "5") 
      setQuestion5(value);

  }

  const processData = () => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ val1: question1, val2: question2, val3: question3, val4: question4,  val5: question5 })
    };
  fetch('http://aldosworld.com/example', requestOptions)
      .then(response => response.json())
      .then(data => { console.log(data); });
  }

  

  return (
    <div>
      <Header title="Aldo Marañon"></Header>

      <br></br>
      <div className="container">
        <div className="row">      
          <div className="col-md-12">
            <h5>Questions</h5>
            <br></br>
          </div>                  
        </div>      

        <Questions handleInputValue={(name, value, id) => { handleInputValue(name, value, id) }} />    

        <div className="row">
          <div className="col-md-12">
            <button className="btn btn-primary float-right" onClick={processData}>Save Survey</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
